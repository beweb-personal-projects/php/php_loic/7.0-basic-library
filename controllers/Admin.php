<?php

class Admin extends GenericsLibrary{
    protected string $name;
    protected int $level;
    
    public function __construct(string $c_name, int $c_level) {
        $this->name = $c_name;
        $this->level = $c_level;
        $this->pushInstanceToClass($this);
    }

}