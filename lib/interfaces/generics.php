<?php 

interface Generics_Objects {
    // General
    public static function count(): int;

    // All instances in a class
    public static function getAll():array;
    public static function getAllInstancesIdByPropertyValue(string $property, mixed $value):array;
    public static function setAll(string $property, mixed $data): void;
    public static function deleteAll(): void;
}

interface Generics_Object {
    // General
    public function pushInstanceToClass(object $obj):void;

    // Object Access
    public function get(): object;
    public function getPropertyValue(string $property):mixed;
    public function getInstanceId():int;
    public function set(string $property, mixed $data):void;

    // Instance Id Access
    public static function setObjectByInstanceId(int $id, string $property, mixed $data):void;
    public static function getObjectByInstanceId(int $id):?object;
    public static function deleteObjectByInstanceId(int $id):void;
}