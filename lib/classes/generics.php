<?php
require(ROOT.'/lib/traits/generics.php');
require(ROOT.'/lib/interfaces/generics.php');

abstract class GenericsLibrary implements Generics_Objects, Generics_Object {
    private static array $class_instanced_objects = [];

    // -------------------------------------------------------------------------------------------------------------------------------- //
    //  [ORIENTED MULTI-OBJECTS METHODS]
    // -------------------------------------------------------------------------------------------------------------------------------- //


    // --------------------------------------------------------------------------------------------------- //
    // General Methods

        public static function count(): int {
            return count(self::$class_instanced_objects);
        }

    // --------------------------------------------------------------------------------------------------- //
    // All instances in a class

        public static function getAll():array {
            return self::$class_instanced_objects;
        }

        public static function getAllInstancesIdByPropertyValue(string $property, mixed $value):array {
            $temp_instances = [];

            foreach (self::$class_instanced_objects as $i => $obj) {
                if($obj[key($obj)]->getPropertyValue($property) === $value)
                    array_push($temp_instances, spl_object_id($obj[key($obj)]));
            }
            return $temp_instances;
        }

        public static function setAll(string $property, mixed $data): void {
            foreach (self::$class_instanced_objects as $i => $obj) {
                $obj[key($obj)]->set($property, $data);
            }
        }

        public static function deleteAll(): void {
            self::$class_instanced_objects = [];
        }


    // -------------------------------------------------------------------------------------------------------------------------------- //
    //  [ORIENTED SINGULAR-OBJECT METHODS]
    // -------------------------------------------------------------------------------------------------------------------------------- //


    // --------------------------------------------------------------------------------------------------- //
    // General Methods

        public function pushInstanceToClass(object $obj):void {
            array_push(self::$class_instanced_objects, [spl_object_id($obj) => $obj]);
        }
    
    // --------------------------------------------------------------------------------------------------- //
    // Access directly to instanced Objects

        public function get():object {
            return $this;
        }
        
        public function getPropertyValue(string $property):mixed {
            return $this->$property;
        }

        public function getInstanceId():int {
            return spl_object_id($this);
        }

        public function set(string $property, mixed $data):void {
            $this->$property = $data;
        }

    // --------------------------------------------------------------------------------------------------- //
    // Access to the object by the instance itself

        public static function getObjectByInstanceId(int $id):?object{
            foreach (self::$class_instanced_objects as $obj) {
                if(key($obj) === $id)
                    return $obj[$id];
            }
            return false;
        }

        public static function setObjectByInstanceId(int $id, string $property, mixed $data):void{
            foreach (self::$class_instanced_objects as $obj) {
                if(key($obj) === $id)
                    $obj[$id]->$property = $data;
            }
        }

        public static function deleteObjectByInstanceId(int $id):void {
            foreach (self::$class_instanced_objects as $i => $obj) {
                if(key($obj) === $id){
                    unset(self::$class_instanced_objects[$i]); // Remove from the array
                    self::$class_instanced_objects = array_values(self::$class_instanced_objects); // Re-ajust the Indexes of the Array
                }
            }
        }

    // --------------------------------------------------------------------------------------------------- //
}