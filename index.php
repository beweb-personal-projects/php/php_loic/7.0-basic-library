<?php 
define('WEBROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_NAME']));
define('ROOT' , str_replace('index.php' , '' , $_SERVER['SCRIPT_FILENAME']));

require('data.php');
require(ROOT.'/lib/classes/generics.php');
// require(ROOT.'/controllers/admin.php');

spl_autoload_register(function ($class_name) {
    include ROOT. "/controllers/" . $class_name . '.php';
});

// ****************************************************************************************
// To add: getObjectById, setObjectById, deleteObjectById, getProperty

// ****************************************************************************************
//  TESTING

echo "<pre>";

$nelle = new Admin("Manelle", 50);
foreach ($data as $i => $value) {
    $current_obj = new Admin($value['name'], $value['level']);
}
$nelle = new Admin("Manelle", 50);

// var_dump(Admin::getInstancedObjects());

// Admin::deleteObjectByInstanceId(2);

// var_dump(Admin::$class_instanced_objects);

var_dump(Admin::getAll());

Admin::setAll("level", 100);

var_dump(Admin::deleteAll());